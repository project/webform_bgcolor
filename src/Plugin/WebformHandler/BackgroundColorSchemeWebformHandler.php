<?php

namespace Drupal\webform_form_bgcolor_scheme\Plugin\WebformHandler;

use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Logger\LoggerChannelFactoryInterface;
use Drupal\webform\Plugin\WebformHandlerBase;
use Drupal\webform\WebformSubmissionConditionsValidatorInterface;
use Drupal\webform\WebformSubmissionInterface;
use Drupal\webform\WebformTokenManagerInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Drupal\Core\StringTranslation\StringTranslationTrait;

/**
 * Webform example handler.
 *
 * @WebformHandler(
 *   id = "custom_webform_template_color_scheme",
 *   label = @Translation("Webform background color scheme"),
 *   category = @Translation("Webform background color scheme"),
 *   description = @Translation("Add background color to webform."),
 *   cardinality = \Drupal\webform\Plugin\WebformHandlerInterface::CARDINALITY_SINGLE,
 *   results = \Drupal\webform\Plugin\WebformHandlerInterface::RESULTS_IGNORED,
 *   submission = \Drupal\webform\Plugin\WebformHandlerInterface::SUBMISSION_REQUIRED,
 * )
 */
class BackgroundColorSchemeWebformHandler extends WebformHandlerBase {

  use StringTranslationTrait;

  /**
   * The token manager.
   *
   * @var \Drupal\webform\WebformTokenManagerInterface
   */
  protected $tokenManager;

  /**
   * {@inheritdoc}
   */
  public function __construct(array $configuration, $plugin_id, $plugin_definition, LoggerChannelFactoryInterface $logger_factory, ConfigFactoryInterface $config_factory, EntityTypeManagerInterface $entity_type_manager, WebformSubmissionConditionsValidatorInterface $conditions_validator, WebformTokenManagerInterface $token_manager) {
    parent::__construct($configuration, $plugin_id, $plugin_definition, $logger_factory, $config_factory, $entity_type_manager, $conditions_validator);
    $this->tokenManager = $token_manager;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->get('logger.factory'),
      $container->get('config.factory'),
      $container->get('entity_type.manager'),
      $container->get('webform_submission.conditions_validator'),
      $container->get('webform.token_manager')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function buildConfigurationForm(array $form, FormStateInterface $form_state) {
    $form['color'] = [
      '#type' => 'fieldset',
      '#title' => $this->t('Webform background color scheme'),
    ];
    $form['color']['color_scheme'] = [
      '#type' => 'color',
      '#title' => $this->t('Select color scheme'),
      '#description' => $this->t("Please choose a color to apply into questionnaire background;"),
      '#default_value' => $this->configuration['color_scheme'],
      '#weight' => 1,
    ];
    $form['#attached']['library'][] = 'webform/webform.element.color';

    return $this->setSettingsParents($form);
  }

  /**
   * {@inheritdoc}
   */
  public function submitConfigurationForm(array &$form, FormStateInterface $form_state) {
    parent::submitConfigurationForm($form, $form_state);
    $this->configuration['color_scheme'] = $form_state->getValue('color', 'color_scheme');
  }

  /**
   * {@inheritdoc}
   */
  public function alterForm(array &$form, FormStateInterface $form_state, WebformSubmissionInterface $webform_submission) {
    if (isset($this->configuration['color_scheme'])) {
      // Add populated background color.
      $custom_style = 'background-color: ' . $this->configuration['color_scheme']['color_scheme'] . ';';
      if (isset($form['#attributes']['style'])) {
        $custom_style = $form['#attributes']['style'] . $custom_style;
      }
      $form['#attributes']['style'] = $custom_style;
    }
  }

}
