CONTENTS OF THIS FILE
---------------------

 * Introduction
 * Configuration
 * Support


INTRODUCTION
------------

The Webform Background Color Scheme module is a webform handler.

 * The primary use case for this module is to:

  - **Change** the inline background color of the webform.


CONFIGURATION
-------------

 * Add a new handler (/admin/structure/webform/manage/{webform}/handlers).

   - Choose a background color from color palate.

   - Save the form & test it.



Support
-------

For additional support or feature request, please create an issue in the
[issue queue](https://www.drupal.org/project/webform_bgcolor) for the
project on drupal.org, or contact the project maintainers.
